<p align="center">
  <img src="images/logo.png" alt="Logo"></img>
  
  <br/>
  <sub>Logo by <a href="https://github.com/lencodigitexer">@lencodigitexer</a></sub>
</p>

<h1 align="center">BridgeMeTheTor</h1>

<h2> Ru </h2>
  <p> Копия <b><a href="https://torscan-ru.ntc.party/">сайта</a></b> (его можно скачать и развернуть локально, в репозитории есть копия сайта, если захотите скачать), который создал <b><a href="https://twitter.com/ValdikSS">ValdikSS</a></b>, переделанное в расширение для браузера, оно помогает искать рабочие мосты для подключению к <b>Tor</b>.
  Расширение, которое позволило бы использовать сайт, как расширение для браузера, сначала идея казалась хорошей, но пообщавшись с разработчиком сайта <b>мы решили закрыть проект</b> (то есть не публиковать его), а общими усилиями создать новое решение позволяющее людям искать и использовать мосты для <b>Tor</b>. Вы можете клонировать репозиторий на компьютер и <b>использовать расширение самостоятельно</b>.</p> 

  <br/>
<h2> Eng </h2>
  <p> Copy <b><a href="https://torscan-ru.ntc.party /">of the site</a></b> (it can be downloaded and deployed locally, there is a copy of the site in the repository if you want to download it) that created <b><a href="https://twitter.com/ValdikSS">ValdikSS</a></b>, converted into a browser extension, it helps to search for working bridges to connect to <b>Tor</b>.
An extension that would allow you to use the site as a browser extension, at first the idea seemed good, but after talking with the developer of the site <b>we decided to close the project </b> (that is, not publish it), and jointly create a new solution that allows people to search and use bridges for <b>Tor</b>. You can clone the repository to a computer and <b>use the extension yourself</b>.</p>

  <br/>
<h2> Credit </h2>
<p>Icons made by:
  <ul>
<li> <a href="https://www.flaticon.com/free-icons/internet" title="internet icons">Internet icons created by Freepik - Flaticon</a> </li>
    <li> <a href="https://iconscout.com/icons">Iconscount</a> </li>
  </ul>

The API is used - <a href="https://onionoo.torproject.org/details?type=relay&running=true&fields=fingerprint,or_addresses" title="internet icons">onionoo.torproject.org</a>
